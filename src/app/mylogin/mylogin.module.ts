import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MyloginRoutingModule } from './mylogin-routing.module';
import { MyloginComponent } from './mylogin.component';

@NgModule({
  declarations: [MyloginComponent],
  imports: [
    CommonModule,
    TranslateModule,
    MyloginRoutingModule
  ]

  
})
export class MyloginModule { }
