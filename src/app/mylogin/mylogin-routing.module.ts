import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MyloginComponent} from './mylogin.component'

const routes: Routes = [
  {
      path: '',
      component: MyloginComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyloginRoutingModule { }
