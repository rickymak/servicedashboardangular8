import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  errorMessage = 'We cannot process Your request at the moment please try again';
  // serverUrl = 'http://3.134.218.36/';
  // serverUrl = 'http://172.104.183.68:7777/';
  serverUrl ='http://localhost:3001/'
  constructor(private http: HttpClient) { }

  login(id) {
    return new Promise((resolve, reject) => {
      this.http.post(this.serverUrl + 'posts/login', id)
        .subscribe(res => {
          if (res) {
            localStorage.setItem('token', res['access']);
            localStorage.setItem('currentUser', JSON.stringify(res));
          }
          resolve(res);
        }, (err) => {
          alert(this.errorMessage);
          reject(err);
        });

    });
  }

  getMainCat() {
    return new Promise((resolve, reject) => {
      this.http.get(this.serverUrl + 'Main/all')
        .subscribe(res => {

         // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }

  MainCategory(fileName) {
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl + 'Main/save', fileName)
       .subscribe(res => {
         resolve(res);
       }, (err) => {

         reject(err);
       });
   });
 }

 getSubCat() {
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl + 'img/all')
       .subscribe(res => {

        // console.log(res['_body']);
         resolve(res);
       }, (err) => {

         reject(err);
       });
   });
 }


 createSubCate(fileName) {
   return new Promise((resolve, reject) => {
     this.http.post(this.serverUrl + 'img/create_blog', fileName)
       .subscribe(res => {

         if (res) {
       localStorage.setItem('currentUser', JSON.stringify(res));
                  }
         resolve(res);
       }, (err) => {

         reject(err);
       });
   });
 }
 getAllStatus() {
   return new Promise((resolve, reject) => {
     this.http.get(this.serverUrl + 'Count/count')
       .subscribe(res => {
         resolve(res);
       }, (err) => {

         reject(err);
       });
   });
 }

}
